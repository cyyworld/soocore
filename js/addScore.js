(function(){
	function addScoreCB(response){
		console.log(response);
		hideLoading();
		if(response.code=="success"){
			 window.history.back();
		}else{
			alert("점수 등록 실패했습니다.");
		}

	}

	function submitScoreClick(){
		var todo = $("#setTodo").text();
		var score = $("#setScore").text();
		var timestamp = (new Date()).getTime();
		var sectionId = dataUtil.SECTION_DATA.sectionId;
		console.log("section id : " + sectionId);
		if(!todo)alert("한 일을 입력해 주세요.");
		else if(!score)alert("점수를 입력해 주세요.");

		if(todo && score){
			showLoading();
			network.addScore(sectionId, score, todo, timestamp, addScoreCB);
		}
	}

	$("#addScore").on("pagecreate",function(){

	});
	$("#addScore").on("pagebeforeshow",function(){
			dataUtil.restore();
		$("#setTodo").empty();
		$("#setScore").empty();
		$("#submitScore").on("click",submitScoreClick);
	});
	$("#addScore").on("pagebeforehide",function(){
			dataUtil.backup();
		$("#submitScore").off("click");
	});


}());
