(function(){
	function createSectionDetail(){
		var sectionData = dataUtil.SECTION_DATA;
		console.log(sectionData);
		if(!sectionData){
			alert("no section data");
				return;
		}

		$("#sectionDetailEmployer").text(sectionData.employer);
		$("#sectionDetailEmployee").text(sectionData.employee);
		$("#sectionDetailGoalScore").text(sectionData.goalScore);
		$("#sectionDetailCurrentScore").text(sectionData.sumScore);
		$("#sectionDetailGoalGift").text(sectionData.goalGift);
		
		var percentage;
		if(sectionData.goalScore > 0){
			percentage = parseInt((sectionData.sumScore/sectionData.goalScore)*100,10);
		}else{
			percentage = 100;
		}
		if(percentage>100)percentage = 100;
		
		if (percentage<1){
			$("#sectionDetailProgressChild").css("width","0%");
		}else if(percentage<15){
			$("#sectionDetailProgressChild").css("width","15%");
		}else{
			$("#sectionDetailProgressChild").css("width",percentage+"%");
		}
		$("#sectionDetailProgressText").text(percentage+"%");
	}

	$("#sectionDetail").on("pagecreate",function(){
		console.log("sectionDetail pagecreate");
	});

	$("#sectionDetail").on("pagebeforeshow",function(){
		dataUtil.restore();
		$("#sectionDetailTodoList").on("click",function(){
			$.mobile.changePage( "#sectionTodoList", { transition: dataUtil.CHANGE_PAGE_TRANSITION});
		});
		createSectionDetail();
	});

	$("#sectionDetail").on("pagebeforehide",function(){
		dataUtil.backup();
		$("#sectionDetailTodoList").off("click");
	});


}());
