(function(){

	var imEmployer = null;
	function setSectionDataCB(response){
		console.log(response);
		
		hideLoading();
		if(response.code=="success" && response.data && response.data[0]){
			showInvite();
			dataUtil.SECTION_DATA = response.data[0];
		}else{
			alert("섹션등록에 실패. 다시시도~");
		}
	}
	function submitSectionClick(){
		var goalGift = $("#goalGift").text();
		var goalPoint = $("#goalPoint").text();
		
		console.log(parseInt(goalPoint,10));
		console.log(isNaN(parseInt(goalPoint,10)));
		if(isNaN(parseInt(goalPoint,10)))alert("목표 점수는 숫자를 입력해 주세요.");
		else if(!goalGift)alert("달성 보상 입력해 주세요.");
		else if(!goalPoint)alert("목표 점수를 입력해 주세요.");
		else if(imEmployer==null)alert("역할을 입력해 주세요.");
		else{
			showLoading();
			if(imEmployer){
				network.setSectionData(goalGift, goalPoint, dataUtil.USER_DATA.emailId, dataUtil.USER_DATA.name, null, null, setSectionDataCB);
			}else{
				network.setSectionData(goalGift, goalPoint, null, null, dataUtil.USER_DATA.emailId,  dataUtil.USER_DATA.name, setSectionDataCB);
			}
		}

		
	}
	function myRoleBtnClick(){
		var id = $(this).attr("id");
		console.log(id);
		if(id === "myEmployerBtn"){
			imEmployer = true;
			$("#myEmployerBtn").css("background-color","rgba(220, 220, 220, 0.2)");
			$("#myEmployeeBtn").css("background-color","transparent");
			
			$("#yourEmployeeBtn").css("background-color","rgba(220, 220, 220, 0.2)");
			$("#yourEmployerBtn").css("background-color","transparent");
			
			$("#myRoleIcon").attr("src","res/employer.png");
			$("#yourRoleIcon").attr("src","res/employee.png");
			
		}else if(id === "myEmployeeBtn"){
			imEmployer = false;
			$("#myEmployeeBtn").css("background-color","rgba(220, 220, 220, 0.2)");
			$("#myEmployerBtn").css("background-color","transparent");
			
			$("#yourEmployerBtn").css("background-color","rgba(220, 220, 220, 0.2)");
			$("#yourEmployeeBtn").css("background-color","transparent");
			
			$("#myRoleIcon").attr("src","res/employee.png");
			$("#yourRoleIcon").attr("src","res/employer.png");
		}
	}
	
	$("#createSection").on("pagecreate",function(){

	});
	$("#createSection").on("pagebeforeshow",function(){
			dataUtil.restore();
		$("#submitSection").on("click",submitSectionClick);
		$("#myRoleName").text(dataUtil.USER_DATA.name);
		$("#yourRoleName").text("초대중...");
		$("#myEmployerBtn, #myEmployeeBtn").on("click",myRoleBtnClick);
		
	});
	$("#createSection").on("pagebeforehide",function(){
		dataUtil.backup();
		$("#submitSection").off("click");
		$("#myEmployerBtn, #myEmployeeBtn").off("click");
	});


}());
