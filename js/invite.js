﻿function showInvite(){

	$("#invite").off("click");
	$("#kakaoShareBtn").off("click");
	$("#invite").on("click",function(){
		hideInvite();
	});
	$("#kakaoShareBtn").on("click",shareKaKao);
	$("#invite").fadeIn();
}
function hideInvite(){
	$("#invite").fadeOut();
	$("#kakaoShareBtn").off("click");
	$("#invite").off("click");
}

function shareKaKao(){
	try{
		Kakao.init('505ef92269ff8e4474cfee4f8bc0752b');
	}
	catch(e){
		console.log(e.message);
	}

		var fromtype = "employee";
		var dataParam;
		if(dataUtil.SECTION_DATA.employerId=="null"){
			dataParam = '?sectionId=' + dataUtil.SECTION_DATA.sectionId+
			'&employee='+dataUtil.SECTION_DATA.employee+
			'&goalGift='+dataUtil.SECTION_DATA.goalGift+
			'&goalScore='+dataUtil.SECTION_DATA.goalScore+
			'&fromtype='+fromtype;
		}else{
			fromtype = "employer";
			dataParam = '?sectionId=' + dataUtil.SECTION_DATA.sectionId+
			'&employer='+dataUtil.SECTION_DATA.employer+
			'&goalGift='+dataUtil.SECTION_DATA.goalGift+
			'&goalScore='+dataUtil.SECTION_DATA.goalScore+
			'&fromtype='+fromtype;
		}

		 Kakao.Link.sendDefault({
			objectType: 'feed',
			content: {
			  title: 'Soocore',
			  description: 'Soocore 초대장 왔습니다',
			  imageUrl: 'http://swantobam.esy.es/soocore/res/background.jpg',
			  link: {
				mobileWebUrl: 'http://swantobam.esy.es/soocore/#inviteResult'+dataParam,
				webUrl: 'http://swantobam.esy.es/soocore/#inviteResult'+dataParam
			  }
			}
      });
	 
	
}