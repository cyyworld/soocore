(function(){
	var ListClickId;
	var ListData;

	function removeList(){
		console.log(ListClickId);
		if(ListClickId){
			var index = parseInt(ListClickId.replace("scoreList",""),10);
			if(index>=0){
				console.log("recordId: " + ListData.scoreRecord[index].recordId);
				showLoading();
				network.removeScore(ListData.scoreRecord[index].recordId, removeScoreCB);
			}else{
				alert("no record index");
			}

		}else{
			alert("no record id");
		}
	}
	function removeScoreCB(response){
		console.log(response);
		if(response.code == "success"){
			network.getSectionData(dataUtil.SECTION_DATA.sectionId, getSectionDataCB);
			$( "#removeConfirm" ).hide();
		}else{
			hideLoading();
			alert("레코드 제거 실패");
		}
	}

	function getSectionDataCB(response){
		console.log(response);
		if(response.code == "success"){
			if(response.data.length == 0){
				hideLoading();
			}else{
				createScoreList(response);
				hideLoading();
			}
		}
	}


	function createScoreList(response){
		$("#scroeRecordView").empty();
		$("#scroeRecordView .todo-list").off("click");
		var index = 0;
		
		ListData = response.data[index];
		dataUtil.SECTION_DATA = ListData;
		dataUtil.backup();
		var currentScore = 0;
		var time, todo, score, fullTime, hour, min, date = "", listId;
		if(!ListData){
			alert("no data!");
		}

		for(var i=0; i < ListData.scoreRecord.length; i++){

			score =  parseInt(ListData.scoreRecord[i].score,10);
			todo = ListData.scoreRecord[i].todo;
			fullTime =  new Date((parseInt(ListData.scoreRecord[i].timestamp,10)));
			hour = fullTime.getHours();
			min = fullTime.getMinutes();
			if(hour<10)hour="0"+hour;
			if(min<10)min="0"+min;
			time = hour+":"+min;
			listId = "scoreList"+i;
			if(date != fullTime.toISOString().slice(0,10)){
				date = fullTime.toISOString().slice(0,10);
				$("#scroeRecordView").append(
						'<div class="todo-list-date">'+
							date +
						'</div>');
			}
			$("#scroeRecordView").append(
			'<div class="todo-list-content" id="'+ listId +'">'+
 				'<div class="todo">' + todo + '</div>'+
				'<div class="score">' + score + '점' + '</div>'+
				'<div class="check" type="checkbox"></div>'+
			'</div>');

			$("#"+listId).on("click",function(){
				ListClickId = $(this).attr("id");
				   $( "#removeConfirm" ).show();
			});
			currentScore += score;
		}
		$("#sectionTodoListCurrentScore").text(currentScore);
	}

	function goAddScore(){
		 $.mobile.changePage( "#addScore", { transition: dataUtil.CHANGE_PAGE_TRANSITION});
	}

	$("#sectionTodoList").on("pagecreate",function(){
		console.log("sectionTodoList create");

	});

	$("#sectionTodoList").on("pagebeforeshow",function(){
		dataUtil.restore();
		console.log("sectionTodoList show");
		showLoading();
		network.getSectionData(dataUtil.SECTION_DATA.sectionId, getSectionDataCB);
		$("#createPointBtn").on("click",goAddScore);
		$("#removeConfirmYes").on("click",removeList);
		$("#removeConfirmNo").on("click",function(){
			 $( "#removeConfirm" ).hide();
		});
	});

	$("#sectionTodoList").on("pagehide",function(){
		dataUtil.backup();
		$("#createPointBtn").off("click");
		$("#scroeRecordView .todo-list").off("click");
		$("#removeConfirmYes").off("click",removeList);
	});


}());
