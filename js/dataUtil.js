
	var dataUtil = {
			USER_DATA : {
					"emailId" : "",
					"name" : "",
					"image" : "",
					"type" : ""
			},
			SECTION_DATA : {
				"sectionId":"",
				"employee":"",
				"employer":"",
				"employerId":"",
				"employeeId":"",
				"goalScore":"",
				"goalGift":"",
				"sumScore":""
			},
			CHANGE_PAGE_TRANSITION : "fade",

		backup : function () {
			window.localStorage.setItem("USER_DATA", JSON.stringify(dataUtil.USER_DATA));
			window.localStorage.setItem("SECTION_DATA", JSON.stringify(dataUtil.SECTION_DATA));
		},

		restore : function () {
	
			if (window.localStorage.USER_DATA) {
				dataUtil.USER_DATA = JSON.parse(window.localStorage.USER_DATA);
			}
			if (window.localStorage.SECTION_DATA) {
				dataUtil.SECTION_DATA = JSON.parse(window.localStorage.SECTION_DATA);
			}
		},
		startKakaoLogin : function(resultCB){
		try{
			Kakao.init('505ef92269ff8e4474cfee4f8bc0752b');
		}catch(e){
			console.log(e.message);
		}
			 Kakao.Auth.login({
		            persistAccessToken: true,
		            persistRefreshToken: true,
		            success: function(authObj) {
		            	console.log(authObj);
		                	Kakao.API.request({
							url: '/v1/user/me',
							success: function(res) {
								console.log(res);
								dataUtil.USER_DATA.emailId = res.kaccount_email;
								dataUtil.USER_DATA.name = res.properties.nickname;
								dataUtil.USER_DATA.image = res.properties.profile_image;
								dataUtil.USER_DATA.type = "kakao";
								dataUtil.backup();
								network.setUserData(dataUtil.USER_DATA.emailId, dataUtil.USER_DATA.name,
										dataUtil.USER_DATA.image, dataUtil.USER_DATA.type, resultCB);
							},
							fail: function(error) {
								console.log(error);
								hideLoading();
							}
						});
		            },
		            fail: function(err) {
		                console.log(err);
		            	hideLoading();
		            }
		        });
		}
		
		
	};
