(function(){
	var RESULT_DATA;

	function sectionListClick(sectionData){
		dataUtil.SECTION_DATA =  sectionData;
		if(sectionData.employerId !== "null" && sectionData.employeeId  !== "null"){
			$.mobile.changePage( "#sectionDetail", { transition: dataUtil.CHANGE_PAGE_TRANSITION});
		}else{
			showInvite();
		}
	
	}
	function drawList(drawData, type, index){
		var listId="";
		if(type=="employee"){
			listId = "employeeList";
		}else{
			listId = "employerList";
		}
		if(drawData.goalScore>0){
			percentage = parseInt(drawData.sumScore/drawData.goalScore*100,10);
		}else{
			percentage = 100;
		}
		if(percentage>100)percentage = 100;
	
		if(percentage<1){
			percentageWidth = 0;
		}else if(percentage<15){
			percentageWidth=15;
		}else{
			percentageWidth = percentage;
		}
		employer = drawData.employer;
		employee = drawData.employee;
		if(drawData.employerId == "null")employer = "초대중...";
		if(drawData.employeeId == "null")employee = "초대중...";
		$("#sectionListContent").prepend("<div id='"+listId+index+"' class='section-list-li employee'>" +
				"<div class='section-list-div'> <img class = 'section-list-emp-icon' src='res/employer.png'/> "+employer+"</div>"+
				"<div class='section-list-div'> <img class = 'section-list-emp-icon' src='res/employee.png'/> "+employee+"</div>"+
				"<div class='progress-bar-parent'><div class='progress-bar-child' style='width:"+percentageWidth+"%;'></div><div class='progress-bar-text'>"+percentage+"%</div></div>"+
				"</div>");

		$("#"+listId+index).on("click",function(e){
			e.stopPropagation();
			console.log($(this).attr("id"));
			var id;
			if($(this).attr("id").indexOf("employeeList") >= 0){
				id = parseInt($(this).attr("id").replace("employeeList",""));
				sectionListClick(RESULT_DATA.employeeData[id]);
			}else{
				id = parseInt($(this).attr("id").replace("employerList",""));
				sectionListClick(RESULT_DATA.employerData[id]);
			}
			console.log(id);
		});
		$("#"+listId+index).on("swipe",function(){
			var id;
			if($(this).attr("id").indexOf("employeeList") >= 0){
				id = parseInt($(this).attr("id").replace("employeeList",""));
				alert(RESULT_DATA.employeeData[id].goalGift);
			}else{
				id = parseInt($(this).attr("id").replace("employerList",""));
				alert(RESULT_DATA.employerData[id].goalGift);
			}
		});
	}
	function createSectionList(){
		hideLoading();
		console.log("createSectionList");
		console.log(RESULT_DATA);

		var employee, employer, gift,score, percentage, percentageWidth = 0;
		$("#sectionListContent").empty();

		for(var i = 0; i<RESULT_DATA.employeeData.length; i++){
			drawList(RESULT_DATA.employeeData[i],"employee",i);
		}

		for(var i=0; i<RESULT_DATA.employerData.length; i++){
			var checkDup = false;
			for(var j=0; j<RESULT_DATA.employeeData.length; j++){
				if(RESULT_DATA.employerData[i].sectionId == RESULT_DATA.employeeData[j].sectionId){
					checkDup = true;
					break;
				}
			}
			if(checkDup)continue;
			drawList(RESULT_DATA.employerData[i],"employer",i);
		}
	}

	function goCreateSection(){
		   $.mobile.changePage( "#createSection", { transition: dataUtil.CHANGE_PAGE_TRANSITION});
	}

	function getAllSectionListCB(resData){
		if(resData.code=="success"){
			RESULT_DATA = resData;
			createSectionList();
		}else{
			hideLoading();
		}
	}

	$("#sectionList").on("pagecreate",function(){
		console.log("sectionList pagecreate");
	});

	$("#sectionList").on("pagebeforeshow",function(){
		$("#createSectionBtn").on("click", goCreateSection);
		dataUtil.restore();
		console.log("sectionList pagebeforeshow");
		showLoading();
		network.getAllSectionList(dataUtil.USER_DATA.emailId, getAllSectionListCB);
	});

	$("#sectionList").on("pagebeforehide",function(){
		$("#createSectionBtn").off("click");
		$("#sectionListContent .section-list-li").off("click");
		$("#sectionListContent .section-list-li").off("swipe");
		console.log("sectionList pagebeforehide");
		dataUtil.backup();
	});


}());
