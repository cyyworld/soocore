﻿(function() {
	var rootURL = "http://swantobam.esy.es/soocore/";

	function requestURL(reqUrl, recieveCB){

		console.log("[URL]" + rootURL+reqUrl);
		   $.ajax({
		   		type : 'POST',
	            url : rootURL+reqUrl,
	            data:{
	                'swan': 'charles'
	            },
	            success : function(response) {
	            	try {
	            		var res = JSON.parse(response);
	            		  if(res){
	  	                	recieveCB(res);
	  	                }
					} catch (e) {
						// TODO: handle exception
					}

	            },
			  	 error:function(request,status,error){
			        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
			       }
			});
	}

	network = (function() {
		return {
			getAllSectionList : function(emailId, recieveCB){
				var url = "php/get_all_section_list.php?" +
				"emailId=" + emailId;
				requestURL(url, recieveCB);
			},
			setUserData : function(emailId, name, image, type, recieveCB){
				var url = "php/set_user_data.php?" +
				"emailId=" + emailId +
				"&name=" + name +
				"&image=" + image +
				"&type=" + type;
				requestURL(url, recieveCB);
			},
			getSectionData : function(sectionId, recieveCB){
				requestURL("php/get_section_data.php?sectionId="+sectionId,recieveCB);
			},
			setSectionData : function(goalGift, goalScore, employerId, employer, employeeId, employee, recieveCB){
				var url = "php/set_section_data.php?" +
						"sectionId=" + (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase() +
						"&goalGift=" + goalGift +
						"&goalScore=" + goalScore +
						"&employerId=" + employerId+
						"&employer=" + employer+
						"&employeeId=" + employeeId+
						"&employee=" + employee;

				requestURL(url,recieveCB);
			},
			addScore : function(sectionId,score, todo, timestamp, recieveCB) {
				var url = "php/add_score.php?" +
				"sectionId=" + sectionId +
				"&recordId=" + (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase() +
				"&score=" + score +
				"&todo=" + todo +
				"&timestamp=" + timestamp;
				requestURL(url, recieveCB);
			},
			removeScore : function(recordId, recieveCB){
				var url = "php/remove_score.php?" +
				"recordId=" + recordId;
				requestURL(url, recieveCB);
			},
			inviteSubmit : function(sectionId, emailId, name, type, recieveCB){
				var url = "php/invite_submit.php?" +
				"&sectionId=" + sectionId +
				"&emailId=" + emailId +
				"&name=" + name+
				"&type=" + type;
				requestURL(url, recieveCB);
			}
		};
	}());

}());
