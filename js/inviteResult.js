﻿(function(){
	var sectionId, invitor, goalGift, goalScore, fromtype, mytyperes;

	function loginSuccessCB(resData){
	console.log(JSON.stringify(resData));
	
	hideLoading();
		if(resData.code == "success"){
			hideLoading();
			getInviteInfo();
			$("#inviteLogin").hide();
			$("#inviteResultUi").show();
		}else{
			alert("다시 시도해 주세요");
		}
	}
	function submitInviteCB(resData){
		if(resData.code=="success"){
			$.mobile.changePage( "#sectionList", { transition: dataUtil.CHANGE_PAGE_TRANSITION});
		}else if(resData.code=="sectionError"){
			alert("이미 만료된 초대장 입니다.");
		}else{
			alert("네트워크 에러");
		}
	}
	function getInviteInfo(){
		console.log("getInviteInfo" );
		var currentLocation = window.location.href;
		var typeres;
		currentLocation = currentLocation.split("?");
		var param = currentLocation[1].split("&");
		
		sectionId = param[0].split("=")[1];
		invitor = decodeURIComponent(param[1].split("=")[1]);
		goalGift = decodeURIComponent(param[2].split("=")[1]);
		goalScore = param[3].split("=")[1];
		fromtype = param[4].split("=")[1];
		
		if(fromtype=="employer"){
			typeres="갑";
			mytyperes = "을";
			$("#inviteData").html("aaaa");
			$("#inviteData").html(typeres + " " +invitor + "의 초대장." + "<br>"+
			typeres+" "+invitor+"는 " + mytyperes + " "+ dataUtil.USER_DATA.name+
			"의 점수가 " + goalScore +"점이 넘으면 아래 약속을 지키겠습니다. <br>" + goalGift);
		}
		else {
			typeres="을";
			mytyperes = "갑";
			
			$("#inviteData").html(typeres + " " +invitor + "의 초대장." + "<br>"+
			mytyperes+" "+dataUtil.USER_DATA.name+"는 " + typeres + " "+ invitor+
			"의 점수가 " + goalScore+"점이 넘으면 아래 약속을 지키겠습니다. <br>" + goalGift);
		}

		$("#inviteSubmit").on("click",function(){
			network.inviteSubmit(sectionId, dataUtil.USER_DATA.emailId, dataUtil.USER_DATA.name, fromtype, submitInviteCB);
		});
	}
	$("#inviteResult").on("pagecreate",function(){
		
	});
	$("#inviteResult").on("pagebeforeshow",function(){
		dataUtil.restore();
		console.log("inviteResult pagebeforeshow - " +dataUtil.USER_DATA.emailId);
		if(dataUtil.USER_DATA.emailId && dataUtil.USER_DATA.emailId !== "null"){
			console.log("loginned" );
			getInviteInfo();
			$("#inviteLogin").hide();
			$("#inviteResultUi").show();
		}else{
			$("#inviteKaKaoLoginBtn").on("click",function(){
				showLoading();
				dataUtil.startKakaoLogin(loginSuccessCB);
			});
			$("#inviteResultUi").hide();
			$("#inviteLogin").show();
		}

	});
	$("#inviteResult").on("pagebeforehide",function(){
		$("#inviteLogin").hide();
		$("#inviteResultUi").hide();
		$("#inviteKaKaoLoginBtn").off("click");
		dataUtil.backup();
	});
}());
